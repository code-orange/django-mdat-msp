from django.db import models
from django.utils.text import slugify


class MdatMspTopics(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=250)
    short_name = models.CharField(max_length=250)

    @property
    def clean_url(self):
        return slugify(self.name, allow_unicode=False)

    class Meta:
        db_table = "mdat_msp_topics"
